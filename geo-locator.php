<?php 

	/*

		CREDIT
		---------------------------------------
		Author: 	Martin Hlavacka
		Contact: 	martinhlavacka@outlook.com 
		Date: 		06.04.2018
				
		LICENSE
		---------------------------------------
		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in all
		copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
		SOFTWARE.

		USAGE
		---------------------------------------
		1. Create a free API token at https://ipstack.com/
		2. Update token value

	*/

	// CREATE NEW OBJECT
	$geoLocation = new GeoLocation();

	// DISPLAY OBTAINED DATA
	echo "<pre>";
		print_r($geoLocation);
	echo "</pre>";

	// CLASS DEFINITION
	class GeoLocation{

		// PROPERTIES
		private $ip;
		private $geo_data;
		private $token;

		// CONSTRUCTOR
		public function __construct(){

			// TOKEN
			$this->token = "YOUR-API-TOKEN-GOES-HERE";

			// STORE USER´S IP ADDRESS
			$this->ip = $this->getIpAddress();
			$this->geo_data = $this->getGeoData($this->ip);

		}

		// METOD GETTING IP ADDRESS
		public function getIpAddress(){

			// GET IP ADDRESS
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}

			// RETURN IP ADDRESS
			return $ip;

		}

		// METHOD GETTING GEO DATA
		public function getGeoData($ip){

			// GET USER´S LOCATION DATA INTO JSON
			$geo_data = json_decode(file_get_contents("http://api.ipstack.com/" . $ip . "?access_key=" . $this->token . "&format=1"));

			// RETURN DATA
			return $geo_data;

		}

	}

?>